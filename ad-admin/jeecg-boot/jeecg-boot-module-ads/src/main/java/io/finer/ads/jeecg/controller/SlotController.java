package io.finer.ads.jeecg.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import io.finer.ads.jeecg.entity.Slot;
import io.finer.ads.jeecg.service.ISlotService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 广告位
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Api(tags="广告位")
@RestController
@RequestMapping("/slot")
@Slf4j
public class SlotController extends JeecgController<Slot, ISlotService> {
	@Autowired
	private ISlotService slotService;

	/**
	 * 分页列表查询
	 *
	 * @param slot
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "广告位-分页列表查询")
	@ApiOperation(value="广告位-分页列表查询", notes="广告位-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Slot slot,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Slot> queryWrapper = QueryGenerator.initQueryWrapper(slot, req.getParameterMap());
		Page<Slot> page = new Page<Slot>(pageNo, pageSize);
		IPage<Slot> pageList = slotService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param slot
	 * @return
	 */
	@AutoLog(value = "广告位-添加")
	@ApiOperation(value="广告位-添加", notes="广告位-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Slot slot) {
		slotService.save(slot);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param slot
	 * @return
	 */
	@AutoLog(value = "广告位-编辑")
	@ApiOperation(value="广告位-编辑", notes="广告位-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Slot slot) {
		slotService.updateById(slot);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告位-通过id删除")
	@ApiOperation(value="广告位-通过id删除", notes="广告位-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		slotService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "广告位-批量删除")
	@ApiOperation(value="广告位-批量删除", notes="广告位-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.slotService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告位-通过id查询")
	@ApiOperation(value="广告位-通过id查询", notes="广告位-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Slot slot = slotService.getById(id);
		if(slot==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(slot);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param slot
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Slot slot) {
        return super.exportXls(request, slot, Slot.class, "广告位");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Slot.class);
    }

}
