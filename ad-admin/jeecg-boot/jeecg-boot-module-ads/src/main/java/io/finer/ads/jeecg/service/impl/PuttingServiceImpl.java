package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.Putting;
import io.finer.ads.jeecg.mapper.PuttingMapper;
import io.finer.ads.jeecg.service.IPuttingService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 广告投放
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class PuttingServiceImpl extends ServiceImpl<PuttingMapper, Putting> implements IPuttingService {

}
