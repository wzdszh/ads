package io.finer.ads.jeecg.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 点击日志
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Data
@TableName("ads_click_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ads_click_log对象", description="点击日志")
public class ClickLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.Integer id;
	/**广告投放ID*/
	@Excel(name = "广告投放ID", width = 15)
    @ApiModelProperty(value = "广告投放ID")
    private java.lang.String putId;
	/**访问的IP*/
	@Excel(name = "访问的IP", width = 15)
    @ApiModelProperty(value = "访问的IP")
    private java.lang.String fromIp;
	/**来自页面URL*/
	@Excel(name = "来自页面URL", width = 15)
    @ApiModelProperty(value = "来自页面URL")
    private java.lang.String fromUrl;
	/**点击链接*/
	@Excel(name = "点击链接", width = 15)
    @ApiModelProperty(value = "点击链接")
    private java.lang.String clickUrl;
	/**访问者*/
	@Excel(name = "访问者", width = 15)
    @ApiModelProperty(value = "访问者")
    private java.lang.String visitor;
	/**访问时间*/
	@Excel(name = "访问时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "访问时间")
    private java.util.Date createTime;
}
