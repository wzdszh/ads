const Joi = require('joi');
const Controllers = require('../controllers');

let clickLog = {
    method: 'get',
    path: '/click/log',
    config: {
        auth: false,
        description: 'Routing with parameters',
        notes: ' api',
        tags: ['api']
    },
    handler: Controllers.clickLog.log
};

module.exports = clickLog;