/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Put', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    slot_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    ad_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    put_type: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    start_datetime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    end_datetime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    show_max: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: '0'
    },
    click_max: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: '0'
    },
    click_countable: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    weight: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    day_times: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    enabled: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    create_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    update_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    show_count: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: '0'
    },
    click_count: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: '0'
    },
    curr_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    curr_show_count: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      defaultValue: '0'
    }     
  }, {
    tableName: 'ads_put'
  });
};
