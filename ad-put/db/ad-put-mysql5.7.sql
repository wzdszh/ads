/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : ads

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-06-21 19:17:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ads_ad
-- ----------------------------
DROP TABLE IF EXISTS `ads_ad`;
CREATE TABLE `ads_ad` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `advertiser_id` varchar(36) DEFAULT NULL COMMENT '广告主ID',
  `advertiser` varchar(100) DEFAULT NULL COMMENT '广告主',
  `type` int(3) NOT NULL COMMENT '类型',
  `size_type_name` varchar(100) DEFAULT NULL COMMENT '尺寸类型',
  `width` int(10) DEFAULT NULL COMMENT '宽 px',
  `height` int(10) DEFAULT NULL COMMENT '高 px',
  `click_url` varchar(1000) DEFAULT NULL COMMENT '点击链接',
  `target_window` varchar(100) DEFAULT NULL COMMENT '目标窗口',
  `file_source_type` int(3) NOT NULL DEFAULT '1' COMMENT '文件来源',
  `template_id` varchar(36) DEFAULT NULL COMMENT '广告模板ID',
  `content` varchar(4000) NOT NULL COMMENT '内容',
  `content_ext` varchar(4000) DEFAULT NULL COMMENT '内容扩展',
  `style` varchar(1000) DEFAULT NULL COMMENT '展示样式',
  `enabled` int(3) NOT NULL DEFAULT '1' COMMENT '启用',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ads_ad
-- ----------------------------
INSERT INTO `ads_ad` VALUES ('demo-flash', 'DEMO-Flash', null, null, '3', null, null, null, 'https://www.baidu.com/', '_blank', '1', null, 'temp/demo.swf', '', null, '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:05:17');
INSERT INTO `ads_ad` VALUES ('demo-image', 'DEMO-图片', null, null, '2', null, null, null, 'https://www.baidu.com/', '_blank', '1', null, 'temp/demo.png', null, null, '1', null, null, '2019-07-23 00:00:00', 'admin', '2020-06-19 16:05:35');
INSERT INTO `ads_ad` VALUES ('demo-image2', 'DEMO-图片2', null, null, '2', null, null, null, 'https://www.baidu.com/', '_blank', '1', null, 'temp/demo2.png', null, null, '1', null, null, '2019-08-01 00:00:00', 'admin', '2020-06-19 16:12:39');
INSERT INTO `ads_ad` VALUES ('demo-image3', 'DEMO-图片3', null, null, '2', null, null, null, 'https://www.baidu.com/', '_blank', '1', null, 'temp/demo3.jpg', null, null, '1', null, null, '2019-08-01 00:00:00', 'admin', '2020-06-19 16:13:20');
INSERT INTO `ads_ad` VALUES ('demo-rich', 'DEMO-富媒体', null, null, '4', null, null, null, '', '_blank', '1', null, '<a href=\"${ILINK1}\" target=\"_blank\">\r\n   <img src=\"${IMG1}\" height=\"200\" width=\"200\" border=\"0\"></img>\r\n</a>\r\n<ul>\r\n    <li><a href=\"${TLINK1}\" target=\"_blank\">${TXT1}</a></li>\r\n    <li><a href=\"${TLINK2}\" target=\"_blank\">${TXT2}</a></li>\r\n</ul>', '[\r\n  {\r\n    \"type\": 2,\r\n    \"cname\": \"${IMG1}\",\r\n    \"cvalue\": \"temp/demo.png\",\r\n    \"source\": 1,\r\n    \"lname\": \"${ILINK1}\",\r\n    \"lvalue\": \"https://www.baidu.com/\",\r\n    \"countable\": 0\r\n  },\r\n  {\r\n    \"type\": 1,\r\n    \"cname\": \"${TXT1}\",\r\n    \"cvalue\": \"点我1\",\r\n    \"lname\": \"${TLINK1}\",\r\n    \"lvalue\": \"https://www.baidu.com\",\r\n    \"countable\": 1\r\n  },\r\n  {\r\n    \"type\": 1,\r\n    \"cname\": \"${TXT2}\",\r\n    \"cvalue\": \"点我2\",\r\n    \"lname\": \"${TLINK2}\",\r\n    \"lvalue\": \"https://www.baidu.com\",\r\n    \"countable\": 1\r\n  }\r\n]', null, '1', null, null, '2019-09-06 00:00:00', 'admin', '2020-06-19 16:01:04');
INSERT INTO `ads_ad` VALUES ('demo-third', 'DEMO-第三方代码', null, null, '5', null, null, null, null, '_blank', '1', null, '<script class=\"AD42bc4e2f-b826-11e9-9ed0-18dbf2568723\" src=\"http://127.0.0.1:8090/js/ad.js\"></script>\r\n<script>ad(\'e\', getCurrentScript());</script>', null, null, '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:02:24');
INSERT INTO `ads_ad` VALUES ('text', 'DEMO-文字', null, null, '1', null, null, null, 'https://www.baidu.com/', '_blank', '1', null, '文字广告<br>点击我', null, 'font-size:20px; text-decoration:none;', '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:04:38');

-- ----------------------------
-- Table structure for ads_channel
-- ----------------------------
DROP TABLE IF EXISTS `ads_channel`;
CREATE TABLE `ads_channel` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ads_channel
-- ----------------------------
INSERT INTO `ads_channel` VALUES ('1272535009213194241', '综合', null, 'admin', '2020-06-15 22:22:43', null, null);
INSERT INTO `ads_channel` VALUES ('1272535120039288834', '新闻', null, 'admin', '2020-06-15 22:23:09', null, null);

-- ----------------------------
-- Table structure for ads_click_log
-- ----------------------------
DROP TABLE IF EXISTS `ads_click_log`;
CREATE TABLE `ads_click_log` (
  `id` int(19) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `put_id` varchar(36) NOT NULL COMMENT '广告投放ID',
  `from_ip` varchar(50) DEFAULT NULL COMMENT '访问的IP',
  `from_url` varchar(1000) DEFAULT NULL COMMENT '来自页面URL',
  `click_url` varchar(1000) DEFAULT NULL COMMENT '点击链接',
  `visitor` varchar(50) DEFAULT NULL COMMENT '访问者',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ads_click_log
-- ----------------------------

-- ----------------------------
-- Table structure for ads_put
-- ----------------------------
DROP TABLE IF EXISTS `ads_put`;
CREATE TABLE `ads_put` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `slot_id` varchar(36) NOT NULL COMMENT '广告位',
  `ad_id` varchar(36) NOT NULL COMMENT '广告',
  `put_type` int(3) NOT NULL COMMENT '投放类型',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `show_max` int(19) NOT NULL DEFAULT '0' COMMENT '展示数不超过',
  `click_max` int(19) NOT NULL DEFAULT '0' COMMENT '点击数不超过',
  `click_countable` int(3) NOT NULL DEFAULT '1' COMMENT '是否统计点击量',
  `weight` int(11) NOT NULL DEFAULT '100' COMMENT '投放权重',
  `day_times` decimal(8,0) NOT NULL DEFAULT '0' COMMENT '每人每天投放次数',
  `enabled` int(3) NOT NULL DEFAULT '1' COMMENT '启用',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `show_count` int(19) NOT NULL DEFAULT '0' COMMENT '展示数',
  `click_count` int(19) NOT NULL DEFAULT '0' COMMENT '点击数',
  `curr_date` datetime DEFAULT NULL COMMENT '当天日期',
  `curr_show_count` int(19) NOT NULL DEFAULT '0' COMMENT '当天展示数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告投放';

-- ----------------------------
-- Records of ads_put
-- ----------------------------
INSERT INTO `ads_put` VALUES ('01', 'demo-embed-image-rotate', 'demo-image', '1', '2019-07-20 00:00:00', '2021-08-23 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-07-23 00:00:00', 'admin', '2020-06-19 16:31:44', '6637', '10', '2020-06-21 00:00:00', '7');
INSERT INTO `ads_put` VALUES ('02', 'demo-embed-image-rotate', 'demo-image2', '1', '2019-08-01 00:00:00', '2021-09-01 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-01 00:00:00', 'admin', '2020-06-19 16:32:25', '6621', '6', '2020-06-21 00:00:00', '7');
INSERT INTO `ads_put` VALUES ('03', 'demo-embed-image-rotate', 'demo-image3', '1', '2019-08-01 00:00:00', '2021-08-01 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-01 00:00:00', 'admin', '2020-06-19 16:33:00', '6627', '5', '2020-06-21 00:00:00', '7');
INSERT INTO `ads_put` VALUES ('04', 'demo-move-image', 'demo-image', '1', '2019-08-11 00:00:00', '2020-07-23 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-12 00:00:00', 'admin', '2020-06-19 16:35:53', '21', '0', '2020-06-19 00:00:00', '12');
INSERT INTO `ads_put` VALUES ('05', 'demo-float-image', 'demo-image', '1', '2019-08-11 00:00:00', '2021-08-12 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-12 00:00:00', 'admin', '2020-06-19 16:35:21', '11', '2', '2019-09-17 00:00:00', '5');
INSERT INTO `ads_put` VALUES ('06', 'ddemo-embed-rich', 'demo-rich', '0', '2019-08-24 00:00:00', '2021-06-19 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:33:41', '470', '119', '2020-06-19 00:00:00', '12');
INSERT INTO `ads_put` VALUES ('07', 'demo-embed-text', 'text', '0', '2018-08-24 00:00:00', '2020-08-26 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:28:42', '31', '6', '2019-09-06 00:00:00', '3');
INSERT INTO `ads_put` VALUES ('08', 'demo-embed-flash', 'demo-flash', '0', '2019-07-26 00:00:00', '2020-08-26 00:00:00', '0', '0', '0', '100', '0', '1', null, null, '2019-08-26 00:00:00', 'admin', '2020-06-19 16:28:19', '231', '20', '2019-09-17 00:00:00', '26');
INSERT INTO `ads_put` VALUES ('09', 'demo-popup-image', 'demo-image', '0', '2019-08-11 00:00:00', '2021-08-12 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-12 00:00:00', 'admin', '2020-06-19 16:36:20', '75', '7', '2020-06-21 00:00:00', '5');
INSERT INTO `ads_put` VALUES ('10', 'demo-embed-third', 'demo-third', '0', '2019-07-28 00:00:00', '2020-08-28 00:00:00', '0', '0', '1', '100', '0', '1', null, null, '2019-08-28 00:00:00', 'admin', '2020-06-19 16:26:27', '44', '8', '2019-09-06 00:00:00', '8');

-- ----------------------------
-- Table structure for ads_show_log
-- ----------------------------
DROP TABLE IF EXISTS `ads_show_log`;
CREATE TABLE `ads_show_log` (
  `id` int(19) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `put_id` varchar(36) NOT NULL COMMENT '广告投放ID',
  `from_ip` varchar(50) DEFAULT NULL COMMENT '访问的IP',
  `from_url` varchar(1000) DEFAULT NULL COMMENT '来自的页面URL',
  `visitor` varchar(50) DEFAULT NULL COMMENT '访问者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19051 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ads_show_log
-- ----------------------------

-- ----------------------------
-- Table structure for ads_show_log_history
-- ----------------------------
DROP TABLE IF EXISTS `ads_show_log_history`;
CREATE TABLE `ads_show_log_history` (
  `id` int(19) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `put_id` varchar(36) NOT NULL COMMENT '广告投放ID',
  `from_ip` varchar(50) DEFAULT NULL COMMENT '访问的IP',
  `from_url` varchar(1000) DEFAULT NULL COMMENT '来自的页面URL',
  `visitor` varchar(50) DEFAULT NULL COMMENT '访问者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='历史展示日志';

-- ----------------------------
-- Records of ads_show_log_history
-- ----------------------------

-- ----------------------------
-- Table structure for ads_size_type
-- ----------------------------
DROP TABLE IF EXISTS `ads_size_type`;
CREATE TABLE `ads_size_type` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `width` int(10) NOT NULL COMMENT '宽 px',
  `height` int(10) NOT NULL COMMENT '高 px',
  `file_types` varchar(100) DEFAULT NULL COMMENT '文件类型',
  `file_max` int(5) DEFAULT NULL COMMENT '文件最大KB',
  `position` varchar(200) DEFAULT NULL COMMENT '位置说明',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告尺寸类型';

-- ----------------------------
-- Records of ads_size_type
-- ----------------------------
INSERT INTO `ads_size_type` VALUES ('2246c75f-d528-11e9-a07a-18dbf2568723', '728*90 通栏', '728', '90', null, '0', null, null, '0801', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('2aea5ece-d467-11e9-aad1-18dbf2568723', '234*60 半幅', '234', '60', null, '0', null, '适用于框架或左右形式主页的广告链接', '0103', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('41240133-d524-11e9-a07a-18dbf2568723', '336*280 大长方形', '336', '280', null, '0', null, null, '0503', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('47218fa5-d50d-11e9-a07a-18dbf2568723', '120*240 竖幅', '120', '240', null, '0', null, null, '0301', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('4c908711-d51f-11e9-a07a-18dbf2568723', '160*600 宽摩天楼', '160', '600', null, '0', null, null, '0402', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('6334a965-d467-11e9-aad1-18dbf2568723', '392*72 横幅二/导航', '392', '72', null, '0', '页面顶部或底部', '主要用于有较多图片展示的广告条，用于页眉或页脚', '0102', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('6d534cd5-d466-11e9-aad1-18dbf2568723', '180*150 长方形', '120', '120', null, '0', null, '', '0501', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('8b61ba61-d467-11e9-aad1-18dbf2568723', '468*60 横幅/旗帜', '468', '60', 'gif、jpg、swf', '16', '页面顶部', '应用最为广泛的广告条尺寸', '0101', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('a2cf20a2-d466-11e9-aad1-18dbf2568723', '120*60 按钮二', '120', '60', 'gif、jpg、swf', '8', null, '主要用于做LOGO使用', '0203', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('aa18f354-d467-11e9-aad1-18dbf2568723', '88*31   小按钮', '88', '31', null, '0', null, '主要用于网页链接，或网站小型LOGO', '0204', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('c80fd81c-d50a-11e9-a07a-18dbf2568723', '300*250 中长方形', '170', '60', 'gif、jpg、swf', '8', null, null, '0502', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('cfbdd49e-d466-11e9-aad1-18dbf2568723', '120*90 按钮一', '120', '90', null, '0', null, '主要应用于产品演示或大型LOGO', '0202', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('d12a5232-d524-11e9-a07a-18dbf2568723', '240*400 竖长方形', '240', '400', null, '0', null, null, '0504', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('e80b4d0d-d51e-11e9-a07a-18dbf2568723', '120*600 摩天楼', '120', '600', null, '0', null, null, '0401', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('f0e0189c-d50b-11e9-a07a-18dbf2568723', '250*250 方形弹出', '250', '250', 'gif、jpg、swf', '40', '首页', null, '0601', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('f4571b31-d466-11e9-aad1-18dbf2568723', '125*125 方形按钮', '125', '125', null, '0', null, '适于表现照片效果的图像广告', '0201', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');
INSERT INTO `ads_size_type` VALUES ('fb85cedd-d526-11e9-a07a-18dbf2568723', '720*300 背投广告', '720', '300', null, '0', null, null, '0701', '2020-06-15 12:43:24', null, '2020-06-15 12:43:24');

-- ----------------------------
-- Table structure for ads_slot
-- ----------------------------
DROP TABLE IF EXISTS `ads_slot`;
CREATE TABLE `ads_slot` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `channel_id` varchar(36) DEFAULT NULL COMMENT '频道',
  `media_type` varchar(20) NOT NULL DEFAULT 'all' COMMENT '终端类型',
  `type` int(3) NOT NULL COMMENT '类型',
  `size_type_name` varchar(100) DEFAULT NULL COMMENT '尺寸类型',
  `width` int(10) NOT NULL COMMENT '宽 px',
  `height` int(10) NOT NULL COMMENT '高 px',
  `file_types` varchar(100) DEFAULT NULL COMMENT '文件类型',
  `file_max` int(5) DEFAULT NULL COMMENT '文件最大KB',
  `position` varchar(200) DEFAULT NULL COMMENT '位置说明',
  `align_h` varchar(50) DEFAULT NULL COMMENT '水平对齐',
  `margin_h` int(5) NOT NULL DEFAULT '0' COMMENT '水平边距 px',
  `align_v` varchar(50) DEFAULT NULL COMMENT '垂直对齐',
  `margin_v` int(10) NOT NULL DEFAULT '0' COMMENT '垂直边距 px',
  `stay_time` int(10) NOT NULL DEFAULT '0' COMMENT '停留时间（秒）',
  `scrolled` int(3) NOT NULL DEFAULT '0' COMMENT '是否跟随滚动条',
  `closable` int(3) NOT NULL DEFAULT '0' COMMENT '是否有关闭按钮',
  `schedule_mode` int(3) NOT NULL COMMENT '调度方式',
  `ad_count` int(10) NOT NULL DEFAULT '1' COMMENT '一次展示广告数',
  `rotate_interval` int(10) NOT NULL DEFAULT '5' COMMENT '轮换间隔（秒）',
  `day_times` decimal(8,3) NOT NULL DEFAULT '0.000' COMMENT '每天显示次数',
  `enabled` int(3) NOT NULL DEFAULT '1' COMMENT '启用',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ads_slot
-- ----------------------------
INSERT INTO `ads_slot` VALUES ('ddemo-embed-rich', 'DEMO-嵌入：富媒体', '1272535009213194241', 'pc', '1', null, '300', '300', null, '0', null, null, '0', null, '0', '0', '0', '0', '11', '1', '5', '0.000', '1', '', null, '2019-08-26 00:00:00', 'admin', '2020-06-19 15:36:45');
INSERT INTO `ads_slot` VALUES ('demo-embed-flash', 'DEMO-嵌入：Flash', '1272535009213194241', 'pc', '1', null, '410', '410', null, '0', null, null, '0', null, '0', '0', '0', '0', '11', '1', '5', '0.000', '1', '', null, '2019-08-26 00:00:00', 'admin', '2020-06-19 15:37:00');
INSERT INTO `ads_slot` VALUES ('demo-embed-image-rotate', 'DEMO-嵌入：图片+轮播', '1272535009213194241', 'pc', '1', 'a', '410', '410', null, '0', null, '1', '1', '1', '1', '0', '0', '0', '11', '3', '5', '0.000', '1', '', '', '2019-07-18 00:00:00', 'admin', '2020-06-19 15:31:29');
INSERT INTO `ads_slot` VALUES ('demo-embed-text', 'DEMO-嵌入：文字', '1272535009213194241', 'pc', '1', null, '100', '100', null, '0', null, null, '0', null, '0', '0', '0', '0', '11', '1', '5', '0.000', '1', '', null, '2019-08-26 00:00:00', 'admin', '2020-06-19 15:36:32');
INSERT INTO `ads_slot` VALUES ('demo-embed-third', 'DEMO-嵌入：第三方代码', '1272535009213194241', 'pc', '1', null, '410', '410', null, '0', null, null, '0', null, '0', '0', '0', '0', '11', '1', '5', '0.000', '1', '', null, '2019-09-06 00:00:00', 'admin', '2020-06-19 15:37:18');
INSERT INTO `ads_slot` VALUES ('demo-float-image', 'DEMO-漂浮：图片', '1272535009213194241', 'pc', '2', 'a', '150', '150', null, '0', null, 'right', '10', 'top', '100', '0', '0', '1', '11', '1', '3', '0.000', '1', '', '', '2019-08-12 00:00:00', 'admin', '2020-06-19 15:32:26');
INSERT INTO `ads_slot` VALUES ('demo-move-image', 'DEMO-漂移：图片', '1272535009213194241', 'pc', '4', null, '150', '150', null, '0', null, null, '0', null, '0', '0', '0', '1', '11', '1', '3', '0.000', '1', '', null, '2019-08-12 00:00:00', 'admin', '2020-06-19 15:36:13');
INSERT INTO `ads_slot` VALUES ('demo-popup-image', 'DEMO-弹窗：图片', '1272535009213194241', 'pc', '3', 'a', '410', '410', null, '0', null, 'center', '0', 'center', '0', '0', '0', '1', '11', '2', '3', '100.000', '1', '', null, '2019-07-19 00:00:00', 'admin', '2020-06-19 15:30:43');

-- ----------------------------
-- Table structure for ads_template
-- ----------------------------
DROP TABLE IF EXISTS `ads_template`;
CREATE TABLE `ads_template` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `namme` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(4000) NOT NULL COMMENT '代码',
  `element` varchar(2000) NOT NULL COMMENT '元素',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告模板';

-- ----------------------------
-- Records of ads_template
-- ----------------------------

-- ----------------------------
-- View structure for ads_putable
-- ----------------------------
DROP VIEW IF EXISTS `ads_putable`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ads_putable` AS select `p`.`id` AS `id`,`p`.`slot_id` AS `slot_id`,`p`.`ad_id` AS `ad_id`,`p`.`show_max` AS `show_max`,`p`.`click_max` AS `click_max`,`p`.`weight` AS `weight`,`p`.`day_times` AS `day_times`,`p`.`show_count` AS `show_count`,`p`.`click_count` AS `click_count`,`p`.`click_countable` AS `click_countable`,if((`p`.`curr_date` = cast(sysdate() as date)),`p`.`curr_show_count`,0) AS `curr_show_count` from `ads_put` `p` where ((case `p`.`put_type` when 0 then TRUE when 1 then (sysdate() between `p`.`start_time` and `p`.`end_time`) when 2 then (`p`.`show_max` > `p`.`show_count`) when 3 then (`p`.`click_max` > `p`.`click_count`) else TRUE end) and (`p`.`enabled` = 1)) ;

-- ----------------------------
-- View structure for ads_visitor_stat
-- ----------------------------
DROP VIEW IF EXISTS `ads_visitor_stat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ads_visitor_stat` AS select `s`.`put_id` AS `put_id`,`s`.`visitor` AS `visitor`,cast(`s`.`create_time` as date) AS `visit_date`,count(`s`.`put_id`) AS `show_count` from `ads_show_log` `s` group by `s`.`put_id`,`s`.`visitor`,cast(`s`.`create_time` as date) ;

-- ----------------------------
-- Event structure for ads_move_show_log
-- ----------------------------
DROP EVENT IF EXISTS `ads_move_show_log`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `ads_move_show_log` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-21 13:34:00' ON COMPLETION PRESERVE ENABLE DO BEGIN
	DECLARE ts TIMESTAMP;
	SET ts = DATE(CURRENT_TIMESTAMP());
	
	INSERT INTO ads_show_log_history(id, put_id, from_ip, from_url, visitor, create_time)
		SELECT l.id, l.put_id, l.from_ip, l.from_url, l.visitor, l.create_time
		  FROM ads_show_log l
		 WHERE l.create_time < ts;
		 
	DELETE FROM ads_show_log 
	 WHERE create_time < ts;	
END
;;
DELIMITER ;
